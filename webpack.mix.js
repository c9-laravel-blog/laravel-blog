let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/admin/js/app.js', 'public/admin/js')
    .js('resources/assets/js/app.js', 'public/js')
    .extract(['vue', 'jquery', 'lodash', 'axios', 'bootstrap', 'popper.js'])
    .styles('resources/assets/admin/css/app.css', 'public/admin/css/app.css')
   .sass('resources/assets/sass/app.scss', 'public/css');
