@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row  justify-content-center">
            <div class="col-md-12">
                <p class="lead">Create Post</p>
                @include('admin.posts.post', ['action' => 'create'])
            </div>
        </div>
    </div>
@endsection