@if ((isset($post) && $post && $action == 'edit') || $action == 'create')
    <div class="card mb-3">
        <div class="card-body">
            <form action="/admin/posts/save" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Upload Image</label>
                <div class="card-img-top mb-2">
                    @if (isset($post) && $post['image_url'] != 'noimage.jpg')
                        <img style="width: 20%" src="{{$post['image_url']}}" />
                    @else
                        <img style="width: 20%" src="/img/no_image_available.jpeg" />
                    @endif
                </div>
                <div class="card-text">
                   <div class="form-group">
                        <label for="image_url">Image</label>
                        <input type="file" class="form-control-file" id="image_url" name="image_url">
                    </div>
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="title">Title</span>
                            </div>
                            <input type="text" class="form-control" name="title" value="{{isset($post) ? $post['title'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="author">Author</span>
                            </div>
                            <input type="text" class="form-control" name="author" value="{{isset($post) ? $post['author'] : ''}}">
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="url">URL</span>
                        </div>
                        <input type="text" class="form-control" name="url" value="{{isset($post) ? $post['url'] : ''}}">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="author">Source</span>
                        </div>
                        <input type="text" class="form-control" name="source" value="{{isset($post) ? $post['source'] : ''}}">
                    </div>
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea class="form-control" id="body" name="body" rows="3">{{isset($post) ? $post['body'] : ''}}</textarea>
                    </div>
                    <blockquote class="blockquote text-right" style="font-size: 0.8rem">
                        <footer class="blockquote-footer">Published at {{isset($post) ? $post['published_at'] : date('Y-m-d H:i:s')}}</footer>
                    </blockquote>
                </div>
                <input type="hidden" name="id" value="{{isset($post) ? $post['post_id'] : ''}}" />
                <input type="hidden" name="action" value="{{$action}}" />
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
            <div class="lead text-right">
                <a href="{{ route('dashboard') }}">Back to Dashboard</a>
            </div>
        </div>
    </div>
@else
    <div class="lead text-center">
        <span>Post not found</span><br />
        <a href="{{ route('dashboard') }}">Back to Dashboard</a>
    </div>
@endif