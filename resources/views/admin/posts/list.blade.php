<div class="container-fluid">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-6">
					<h2>Manage <b>Posts</b></h2>
				</div>
				<div class="col-sm-6 text-right">
					<a href="{{ route('create_view') }}" class="btn btn-success"><i class="material-icons">&#xE147;</i> <span>Add New Post</span></a>
				</div>
            </div>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Source</th>
                    <th>URL</th>
                    <th>Author</th>
                    <th>Published At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post['title'] }}</td>
    					<td>{{ $post['source'] }}</td>
                        <td><a href="{{ $post['url'] }}"  target="_blank">View</a></td>
                        <td>{{ $post['author'] }}</td>
                        <td>{{ $post['published_at'] }}</td>
                        <td>
                            <a href="{{ route('edit_view', ['id' => $post['post_id']]) }}" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                            <a href="{{ route('delete_post', ['id' => $post['post_id']]) }}" class="delete"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- Delete Modal HTML -->
<div id="deletePostModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="deleteModal">
				<div class="modal-header">						
					<h4 class="modal-title">Delete Post</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">					
					<p>Are you sure you want to delete these Posts?</p>
					<p class="text-warning"><small>This action cannot be undone.</small></p>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-danger" value="Delete">
				</div>
			</form>
		</div>
	</div>
</div>