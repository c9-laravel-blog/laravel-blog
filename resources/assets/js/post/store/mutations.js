export default {
  ['GET_ALL'](state, posts) {
    state.posts = posts;
  },
  ['GET'](state, post) {
    state.currentPost = post;
  }
}