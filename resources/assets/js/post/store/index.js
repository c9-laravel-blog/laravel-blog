import Vue from 'vue';
import Vuex from 'vuex';

import state from './state'; // note's state
import actions from './actions'; //note's action
import mutations from './mutations'; // note's mutation

Vue.use(Vuex);

const store = new Vuex.Store({
  state,
  actions,
  mutations,
});

export default store;