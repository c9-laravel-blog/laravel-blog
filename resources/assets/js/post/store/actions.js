import axios from 'axios';

export default {
  getAll({commit}, payload) {
    const page = payload && payload.current_page ? '?page='+payload.current_page : '';
    const getUri = '/api/posts' + page; // payload.current_page
    
    return axios.get(getUri)
      .then((response) => {
        commit('GET_ALL', response.data);
      })
      .catch();
  },
  
  getOne({commit}, payload) {
    return axios.get(`/api/posts/${payload.slug}`)
      .then((response) => {
        commit('GET', response.data);
      })
      .catch();
  }
}