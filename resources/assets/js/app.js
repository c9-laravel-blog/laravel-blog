import Vue from 'vue';
import VueRouter from 'vue-router';
import ListPosts from './components/ListPosts.vue';
import routes from './routes';
import store from './post/store/index';

Vue.use(VueRouter);

const router = new VueRouter({
    routes
});

window.events = new Vue();

window.flash = function(message, type = 'success') {
    window.events.$emit('flash', message, type);
};

const app = new Vue({
  router,
  store
}).$mount('#app')