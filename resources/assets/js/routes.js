import ListPosts from './components/ListPosts';
import ViewPost from './components/ViewPost';


const routes = [
  { path: '/', name: 'list-posts', component: ListPosts },
  { path: '/post/:slug', name: 'view-post', component: ViewPost },
  { path: '*', redirect: '/' },
];

export default routes;