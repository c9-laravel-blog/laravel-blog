<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Resources\PostResource;
use App\Post;

class NewsAPI {
    private $client;
    private $postResource;
    
    private $url = "https://newsapi.org/v2/top-headlines";
    private $apiKey = null;
    
    public function __construct() {
        $this->client = new Client([
            'base_uri' => $this->url,
            'timeout' => 2.0
        ]);
        
        $this->apiKey = env('NEWS_API');
    }
    
    public function pull($amount) {
        $response = $this->client->get($this->url, ['query' => [
            'language' => env('APP_LANG'),
            'apiKey' => $this->apiKey,
            'pageSize' => $amount
        ]]);
        
        if ((int)$response->getStatusCode() != 200) {
            throw new GuzzleException("Unable to pull data");
        }
        
        $rawData = $response->getBody();
        $jsonData = json_decode($rawData, true);
        
        $this->saveData($jsonData);
        
        return true;
    }
    
    private function saveData($jsonData) {
        foreach($jsonData['articles'] as $post) {
            Post::create(
                PostResource::make($post)->resolve()    
            );
        }
    }
}