<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PostResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->resource['title'],
            'slug' => str_slug($this->resource['title']),
            'body' => !empty($this->resource['description']) ? $this->resource['description'] : 'No Description',
            'source' => $this->resource['source']['name'],
            'url' => $this->resource['url'],
            'image_url' => !empty($this->resource['urlToImage']) ? $this->resource['urlToImage'] : 'noimage.jpg',
            'author' => (!empty($this->resource['author']) ? $this->resource['author'] : 'Unknown'),
            'published_at' => strtotime($this->resource['publishedAt'])
        ];
    }
}