<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;
    public $primaryKey = 'post_id';
    protected $dates = ['published_at'];
    protected $fillable = ['title', 'slug', 'body', 'source', 'url', 'image_url', 'author', 'published_at'];
}
