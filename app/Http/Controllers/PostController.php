<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function index() {
        $posts = Post::paginate(10);
        
        return response()->json($posts);
    }
    
    public function show($slug, Request $request) {
        $post = Post::where('slug', $slug)->first();
        
        if (is_null($post)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Requested post not found'
            ]);
        }
        
        return response()->json($post);
    }
}
