<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;

class AdminController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
        $posts = $this->listPosts();
        
        return view('admin.admin', [
            'posts' => $posts,
            'total' => sizeof($posts)
        ]);
    }
    
    public function editPost($id, Request $request) {
        $post = Post::find($id);
        
        $data = is_null($post) ? false : $post;
        
        return view('admin.posts.edit', ['post' => $data]);
    }
    
    public function createPost() {
        return view('admin.posts.create');
    }
    
    public function deletePost($id, Request $request) {
        if (auth()->check()) {
            Post::destroy($id);
            
            return redirect()->route('dashboard');
        } else {
            response()->json([
                'status' => 'error',
                'message' => 'Unathorized'
            ]);
        }
    }
    
    private function listPosts() {
        return Post::all();
    }
    
    public function savePost(Request $request) {
        $action = $request->input('action');
        
        if ($action == 'create') {
            $post = new Post();
        } else {
            $post = Post::find($request->input('id'));
        }
        
        $post_data = $request->all();
        
        // @TODO: validation rules applied/checked here if empty/wrong format, skipping for the sake for the main task mission
        $post->title = $post_data['title'];
        $post->author = $post_data['author'];
        $post->source = $post_data['source'];
        $post->body = $post_data['body'];
        $post->url = $post_data['url'];
        $post->slug = str_slug($post_data['title']);
        $post->published_at = date('Y-m-d H:i:s');
        
        if ($request->hasFile('image_url')) {
            $post->image_url = "/" . $request->file('image_url')->store('public/img');
            $post->image_url = str_replace("public", "storage", $post->image_url);
        } else if ($action == 'create') {
            $post->image_url = 'noimage.jpg';
        }
        
        $post->save();
        
        return redirect()->route(
            'edit_view', [$post]
        );
    }
    
    private function uploadImage(Request $request) {
        if ($request->hasFile('image')) {
            $image      = $request->file('image');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->stream();

            Storage::disk('local')->put('img/'.$fileName, $img, 'public');
        }
    }
}