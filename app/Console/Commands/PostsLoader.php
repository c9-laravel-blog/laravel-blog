<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\NewsAPI;

class PostsLoader extends Command {
    protected $signature = "posts:get {amount}";
    protected $description = "Pull desired amount of posts from HubSpot Test API";
    protected $semantics = null;
    protected $amount = 0;
    
    public function __construct(NewsAPI $blogs) {
        parent::__construct();
        
        $this->blogs = $blogs;
    }
    
    public function handle() {
        $amount = intval($this->argument('amount'));
        
        if ($amount < 0 || $amount > 100) {
            $this->error("Argument 'amount' must be an integer and should not exceed 100.");
            return false;
        }
        
        $response = $this->blogs->pull($amount);
        
        if ($response) {
            $this->info("All posts have been pulled and saved to local DB");
        }
    }
}
