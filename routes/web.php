<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/api')->group(function() {
    Route::resource('posts', 'PostController');    
});

Route::prefix('/admin')->group(function() {
    Route::get('/dashboard', 'AdminController@index')->name('dashboard');
    Route::get('/posts/edit/{id}', 'AdminController@editPost')->name('edit_view');
    Route::get('/posts/create', 'AdminController@createPost')->name('create_view');
    Route::post('/posts/save', 'AdminController@savePost');
    Route::get('/posts/delete/{id}', 'AdminController@deletePost')->name('delete_post');
    
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout'); 
});
